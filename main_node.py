from flask import Flask, jsonify, request
from collections import namedtuple
from math import ceil
import requests
import json
import re

# must install requests and flask libraries
app = Flask(__name__)
brapi_endpoints = [  # URLs of different BrAPI endpoints to be queried
    'http://localhost:5001/institution_A/',
    'http://localhost:5001/institution_B/',
    'http://localhost:5001/institution_C/',
    'http://localhost:5001/institution_D/'
]  # note that these will actually be completely different servers


@app.route('/')
def api_root():
    return 'Central server'


@app.route('/central/brapi/v1')
def api_brapi():
    return 'BrAPI discovery node'


@app.route('/central/brapi/v1/<query_domain>')
def discover_and_forward(query_domain):
    """ Fetch data points per institution.
        Send requests to each known BrAPI endpoint, with pageSize=1. """
    # get query string, e.g. 'trials?' followed by GET parameters
    query_string = request.full_path.split('/brapi/v1/', 1)[1]
    # get pageSize and page requested by the user
    user_page = request.args.get('page', default=0, type=int)
    user_pageSize = request.args.get('pageSize', default=5, type=int)
    if user_pageSize < 1: user_pageSize = 5
    if user_page < 0: user_page = 1

    # set pageSize parameter to 1, page to 0
    query_1result = re.sub(r'pageSize(=.*?)(&|$)',
                           r'pageSize=1\2', query_string)
    query_1result = re.sub(r'page(=.*?)(&|$)', r'page=0\2', query_1result)

    # Gather information from each node: how many results does each have?
    nodes = list()
    URL_info = namedtuple('URL_info', ['url', 'count'])
    for url in brapi_endpoints:
        response = requests.get(url + 'brapi/v1/' + query_1result)
        content = json.loads(response.content.decode())
        resultCount = content['metadata']['pagination']['totalCount']
        nodes.append(URL_info(url, resultCount))

    # order by descending number of results
    nodes = sorted(nodes, key=(lambda item: item.count), reverse=True)

    # Calculate pages per institution based on pageSize given by user.
    # Then calculate the page range per URL, until the institution page that
    # corresponds to the overall page is determined.
    lastPage = -1
    for node in nodes:
        result_pages = ceil(float(node.count) / user_pageSize)
        if result_pages < 1: result_pages = 1
        firstPage = lastPage + 1
        lastPage = firstPage + result_pages - 1
        if user_page <= lastPage:
            # adjust page parameter and set URL to form final query to node
            nodeQuery = re.sub(r'page(=.*?)(&|$)',
                               r'page=' + str(user_page - firstPage) + r'\2',
                               query_string)
            response = requests.get(node.url + 'brapi/v1/' + nodeQuery)
            break
    else:
        # page out of result range
        return jsonify({'data': 'no result'})

    return jsonify(json.loads(response.content.decode()))

if __name__ == '__main__':
    app.run()
