# BrAPI discovery node concept

A concept for a Breeding API discovery node.
Written in Python, based on a Flask server.

Requires `requests` and `flask` libraries, runs on Python 2.7+ and 3.x.</br>
The python file `endpoints.py` must run for `main_node.py` to function.

## `endpoints.py`
The module contains a server to represent the BrAPI end points at various
institutions. The data it provides is obviously generated and only a
placeholder for real information, but the main structure of the response is
the same. This can only be skipped if URLs to real BrAPI endpoints are provided.

For the purposes of this example, a BrAPI endpoints reside at
[http://localhost:5001/`[institution_name]`/brapi/v1](http://localhost:5001/institution_name/brapi/v1)

The institution names which provide generated results are
`institution_A`, `institution_B` and `institution_C`. The number of results
to be generated as well as the respective institution names are defined
[here](endpoints.py#L66-70). Parameters supported are `page` and `pageSize`.


**Examples:**
[http://localhost:5001/institution_A/brapi/v1/trials?page=0&pageSize=5](http://localhost:5001/institution_A/brapi/v1/trials?page=0&pageSize=5)

[http://localhost:5001/institution_B/brapi/v1/trials?page=0&pageSize=3](http://localhost:5001/institution_B/brapi/v1/trials?page=0&pageSize=3)


## `main_node.py`
This is the actual aggregator/discovery node. The user query is relayed to each
provided BrAPI endpoint, and the results are presented to the user as per the
`page` and `pageSize` parameters provided.

**Example:**
[http://localhost:5000/central/brapi/v1/trials?page=1&pageSize=4](http://localhost:5000/central/brapi/v1/trials?page=1&pageSize=4)