from flask import Flask, jsonify, request
from copy import deepcopy
from math import ceil


"""
    This server imitates BrAPI endpoints at various institutions.
"""

app = Flask(__name__)
metadata = {'pagination': {}, 'datafiles': [], 'status': 200}


def make_response(totalCount, page, pageSize, sourceURL):
    """ Constructs the JSON object to be returned by the query.
        Pagination and data generation take place here. """
    response = {'result': {'data': []},
                'metadata': deepcopy(metadata)}

    # pagination
    if pageSize < 1: pageSize = 5
    from_item = page * pageSize
    to_item = (page + 1) * pageSize
    page_number = int(ceil(float(totalCount) / pageSize))
    response.update({
        'metadata': {'source': sourceURL,
                     'pagination': {'totalPages': page_number,
                                    'pageSize': pageSize,
                                    'currentPage': page,
                                    'totalCount': totalCount}}
        })

    for i in range(from_item, to_item):
        if i >= totalCount:
            break
        temp_item = {
            'trialDbId': 'Example trialDbId ' + str(i + 1),
            'trialName': 'Example trialName ' + str(i + 1),
            'etc': '...'
        }
        response['result']['data'].append(temp_item)
    return response


@app.route('/')
def api_root():
    return 'Root for BrAPI endpoints, which will actually be separate servers'


@app.route('/<institution_name>')
def api_institution(institution_name):
    return 'Website for ' + institution_name


@app.route('/<institution_name>/brapi/v1')
def api_brapi(institution_name):
    return 'BrAPI endpoint for ' + institution_name


@app.route('/<institution_name>/brapi/v1/<query_domain>')
def api_query(institution_name, query_domain):
    """ Imitate a query, but with only a few elementary attributes to be
        returned. The request accepts page as well as pageSize parameters.
        Only the three institutions below hold data. """
    # set the number of total items (totalCount) per institution
    total_items = {  # 0 results for any institution not in list
        'institution_A': 2,
        'institution_B': 7,
        'institution_C': 3
    }.get(institution_name, 0)
    response = make_response(total_items,
                             request.args.get('page', default=0, type=int),
                             request.args.get('pageSize', default=5, type=int),
                             'http://localhost:5001/' + institution_name +
                             '/brapi/v1')
    return jsonify(response)


if __name__ == '__main__':
    app.run(host='localhost', port=5001)
